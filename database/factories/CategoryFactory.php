<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Category;

class CategoryFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker()->name(),
            'products' => $this->faker()->products(),
            'image' => $this->faker()->image('public/storage/images', 640, 480, null, false),
            'descriptions' => $this->faker()->descriptions(),
        ];
    }
}
