<x-app>
    <!-- Table Start  -->
    <div class="table-area ">
     <div class="container-fluid pt-5">
         <nav aria-label="breadcrumb">
             <ol class="breadcrumb">
               <li class="breadcrumb-item"><a href="category-list.html">Category</a></li>
               <li class="breadcrumb-item active" aria-current="page">Category-list</li>
             </ol>
           </nav>
     </div>
     <div class="container-fluid">
         <div class="row">
             <div class="col-md-6">
                 <div class="cat-list-left">
                     <h2>Student List</h2>
                 </div>
             </div>
             <div class="col-md-6">
                 <div class="cat-list-right">
                     <a href="{{ route('screate') }}">Add Category</a>
                 </div>
             </div>
             
         </div>
     </div>
   <div class="container-fluid pb-3 px-3">
     <table class="table cat-list-img">
       <thead>
         <tr>
           <th scope="col">Id</th>
           <th scope="col">name</th>
           <th scope="col">DOF</th>
           <th scope="col">gender</th>
           <th scope="col">hobbies</th>
           <th scope="col">Nationality</th>

           <th class="cat-action" scope="col">Action</th>
         </tr>
       </thead>
       <tbody>
       @foreach ($student as $item)
           
      
         <tr>
           <th scope="row">{{ $loop->iteration }}</th>
           <td>{{ $item->name }}</td>
            <td>{{$item->dof}}</td>
            <td>{{ $item->gender }}</td>
            <td>{{ $item->food }}</td>
            <td>{{ $item->nationality }}</td>
           <td><a class="bt-1" href="{{ route('sdelete',$item->id) }}">Delete</a> <a class="bt-2" href="{{ route('sedit',$item->id) }}">Edit</a> <a class="bt-3" href="{{ url('view-category') }}">View</a></td>
         </tr>
    
         @endforeach
       </tbody>
     </table>
   </div>
 </div-table-area>
 <!-- Table End  -->
 
 </x-app>