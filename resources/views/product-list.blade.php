<x-app>
   <!-- Table Start  -->
   <div class="table-area ">
    <div class="container-fluid pt-5">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="product-list.html">Product</a></li>
              <li class="breadcrumb-item active" aria-current="page">Product-list</li>
            </ol>
          </nav>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <div class="cat-list-left">
                    <h2>Product List</h2>
                </div>
            </div>
            <div class="col-md-6">
                <div class="cat-list-right">
                    <a href="{{ url('product-add') }}">Add Product</a>
                </div>
            </div>
        </div>
    </div>
  <div class="container-fluid pb-3 px-3">
    <table class="table cat-list-img">
      <thead>
        <tr>
          <th scope="col">Id</th>
          <th scope="col">Images</th>
          <th scope="col">Name</th>
          <th scope="col">Category</th>
          <th scope="col">price</th>
          <th class="cat-action" scope="col">Action</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th scope="row">1</th>
          <td><img src="admin/images/fish-cat.jpg" alt="fish-cat"></td>
          <td>Organic Surma Dry Fish</td>
          <td>Fish</td>
          <td>1295 tk</td>
          <td><a class="bt-1" href="#">Delete</a> <a class="bt-2" href="#">Edit</a> <a class="bt-3" href="{{ url('view-product') }}">View</a></td>
        </tr>
        <tr>
          <th scope="row">2</th>
          <td><img src="admin/images/fruit-cat.jpg" alt="fruit-cat"></td>
          <td>Red Lady Papaya (পেঁপে)</td>
          <td>Fruits</td>
          <td>130 Tk</td>
          <td><a class="bt-1" href="#">Delete</a> <a class="bt-2" href="#">Edit</a> <a class="bt-3" href="{{ url('view-category') }}">View</a></td>
        </tr>
      </tbody>
    </table>
  </div>
</div-table-area>
<!-- Table End  -->
</x-app>