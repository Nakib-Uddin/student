<x-app>
  <!-- Dashbord Area Start -->
  <div class="items">
    <div class="container-fluid">
      <div class="row py-3">
        <div class="col-md-3">
          <div class="single-item">
            <div class="content-item">
              <h5>54</h5>
              <p>Custom</p>
            </div>
            <i class="fa-solid fa-users"></i>
          </div>
        </div>
        <div class="col-md-3">
          <div class="single-item">
            <div class="content-item">
              <h5>104</h5>
              <p>Products</p>
            </div>
            <i class="fa-solid fa-briefcase"></i>
          </div>
        </div>
        <div class="col-md-3">
          <div class="single-item">
            <div class="content-item">
              <h5>34</h5>
              <p>Orders</p>
            </div>
            <i class="fa-solid fa-bus"></i>
          </div>
        </div>
        <div class="col-md-3">
          <div class="single-item">
            <div class="content-item">
              <h5>320</h5>
              <p>Income</p>
            </div>
            <i class="fa-solid fa-money-check-dollar"></i>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Dashbord Area End -->
</div>
<!-- Main Content Area End -->

</x-app>