<x-app>
    <div class="main-content-area">
  
      <!-- View Product Area Start  -->
  <div class="product-section section py-4">
      <div class="container-fluid pt-5">
          <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="category-list.html">Category</a></li>
                <li class="breadcrumb-item active" aria-current="page">Category-Add</li>
              </ol>
            </nav>
      </div>
      <div class="container">
          <div class="row">
              <div class="col-md-12 col-12 mb-30">
                  <div class="categori-table">
                      <form action="{{ route('update',$category->id) }}" method="post" enctype="multipart/form-data">
                          {{ csrf_field() }}
                          <table>
                              <thead>
                                  <tr>
                                      <th><label for="name">Category Name</label></th>
                                      <th><label for="image">Images</label></th>
                                      
                                  </tr>
                              </thead>
                              <tbody>
                                  <tr>
                                      <td><input type="text" value="{{ $category->name }}" name="name" id="ccategory"></td>
                                      <td><input type="file" value="{{ $category->image }}" name="image" id="cimages"></td>
                                  </tr>
                                  <tr>
                                      <td><input class="csubmit" type="submit" name="csubmit" id="csubmit" value="Save Category"></td>
                                  </tr>
                              </tbody>
                          </table>
                      </form>
                  </div>
              </div>
          </div>
    </div>
      <!-- View Product Area End  -->
  
  </div>
  <!-- Main Content Area End -->
  </x-app>